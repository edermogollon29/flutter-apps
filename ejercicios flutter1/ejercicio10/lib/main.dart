import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ejemplos flutter',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hola Flutter'),
        ),
        body: Center(
          child: Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
                color: Colors.green,
                border: Border.all(
                  color: Colors.brown,
                  width: 1,
                ),
                borderRadius: BorderRadius.all(Radius.circular(15))),
            child: Text(
              'Hola',
              style: TextStyle(fontSize: 25),
            ),
            padding: EdgeInsets.only(left: 20),
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(App());
}
