import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ejemplos flutter',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hola Flutter'),
        ),
        body: Container(
          width: 200,
          height: 200,
          color: Colors.red,
          child: Text(
            'Hola',
            style: TextStyle(fontSize: 25),
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(App());
}
