import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ejemplos flutter',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hola Flutter'),
        ),
        body: Center(
          child: Container(
            margin: EdgeInsets.all(30),
            decoration: BoxDecoration(
                color: Colors.green,
                border: Border.all(
                  color: Colors.brown,
                  width: 1,
                ),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(15),
                  bottomLeft: Radius.circular(15),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black45,
                    offset: Offset(6, 6),
                    blurRadius: 4,
                  )
                ]),
            child: Text(
              'Hola',
              style: TextStyle(fontSize: 25),
            ),
            padding: EdgeInsets.only(left: 10),
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(App());
}
