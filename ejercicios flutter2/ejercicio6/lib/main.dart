import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ejemplos flutter',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hola Flutter'),
        ),
        body: Container(
          width: 100,
          height: 100,
          decoration: FlutterLogoDecoration(),
          child: Text(
            'Hola',
            style: TextStyle(fontSize: 25),
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(App());
}
