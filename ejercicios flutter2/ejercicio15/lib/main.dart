///Mas de una imagen con ListView
///
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Inicio(),
    );
  }
}

class Inicio extends StatefulWidget {
  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Mi App"),
        ),
        body: ListView(children: <Widget>[
          Container(
            padding: EdgeInsets.all(20.0),
            child: Image.network(
                "https://images-cdn.9gag.com/photo/aYK8R1m_460s.jpg"),
          ),
          Container(
            padding: EdgeInsets.all(20.0),
            child: Image.network(
                "https://images-cdn.9gag.com/photo/aYK8R1m_460s.jpg"),
          ),
          Container(
            padding: EdgeInsets.all(20.0),
            child: Image.network(
                "https://images-cdn.9gag.com/photo/aYK8R1m_460s.jpg"),
          ),
          Container(
            padding: EdgeInsets.all(20.0),
            child: Image.network(
                "https://images-cdn.9gag.com/photo/aYK8R1m_460s.jpg"),
          ),
          Container(
            padding: EdgeInsets.all(20.0),
            child: Image.network(
                "https://images-cdn.9gag.com/photo/aYK8R1m_460s.jpg"),
          )
        ]));
  }
}
