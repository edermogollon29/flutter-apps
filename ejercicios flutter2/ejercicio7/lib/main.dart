import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ejemplos flutter',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hola Flutter'),
        ),
        body: Center(
          child: Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.brown,
                width: 6,
              ),
            ),
            child: Text(
              'Hola',
              style: TextStyle(fontSize: 25),
            ),
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(App());
}
