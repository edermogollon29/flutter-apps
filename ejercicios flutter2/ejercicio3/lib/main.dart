import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      title: 'Ejemplo Flutter',
      theme: ThemeData(primarySwatch: Colors.deepOrange),
      home: Scaffold(
        appBar: AppBar(
          title: Icon(Icons.add_alarm),
        ),
        body: Center(
          child: Text('Hola, Flutter App'),
        ),
      )));
}
